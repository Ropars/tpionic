import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from './users.service'

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  public users:[];
  public user: string;

  constructor(public router: Router, private UsersService: UsersService) { }

  ngOnInit() {
    this.getUsers()
  }

  getUsers(){
    this.UsersService.getUsers().then(res => {
      this.users = res.users;
    }, err => {
      console.error('Load users OK', err);
    });  
  }

}
