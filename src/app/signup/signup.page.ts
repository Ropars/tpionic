import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SignupService } from './signup.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {



  public signupForm = new FormGroup({
    username: new FormControl('',Validators.compose([Validators.required])),
    password: new FormControl('',Validators.compose([Validators.required])),
    photoUrl: new FormControl('',Validators.compose([Validators.required]))
  });

  goBack() {
    this.location.back();
  }

  doSignUp(){
    console.log(`doSignup ${this.signupForm.value.username}`);
      this.signupService.doSignUp(
        this.signupForm.value.username,
        this.signupForm.value.password,
        this.signupForm.value.photoUrl)
        .then((res) =>{
          console.log('result from service signup OK', res);
          this.goBack()
        }).catch(err =>{
          console.log('result from service signup NOK',err);
        })
  }

  constructor(public location: Location ,public signupService: SignupService) { }

  ngOnInit() {
  }

}
