import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabPage } from './tab.page';

const routes: Routes = [
  {
    path: 'tab',
    component: TabPage,
    children:
      [
        {
          path: 'messages',
          children:
            [
              {
                path: '',
                loadChildren: '../messages/messages.module#MessagesPageModule'
              }
            ]
        },
        {
          path: 'users',
          children:
            [
              {
                path: '',
                loadChildren: '../users/users.module#UsersPageModule'
              }
            ]
        },
        {
          path: '',
          redirectTo: '/tab/messages',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: '/tab/tab/messages',
    pathMatch: 'full'
  }
];

@NgModule({
  imports:
    [
      RouterModule.forChild(routes)
    ],
  exports:
    [
      RouterModule
    ]
})
export class TabPageRoutingModule {}
