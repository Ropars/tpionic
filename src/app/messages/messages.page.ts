import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {
  public messages:[];
  public message: string;

  constructor(public router: Router ,private messagesService: MessagesService) { }

  ngOnInit() {
    this.getMessages()
  }

  getMessages() {
    this.messagesService.get().then(res => {
      this.messages = res.messages;
    }, err => {
      console.error('Load messages NOK', err);
    });
  }

  postMessage() {
    if (this.message) {
      this.messagesService.post(this.message).then(res => {
        this.getMessages();
      }, err => {
        console.error('post message NOK', err);
      });
    }
  }

}
